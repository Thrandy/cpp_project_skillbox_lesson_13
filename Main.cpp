#include <iostream>
#include "Helpers.h"


// Function for reading  number and returning it with console
int NumberReading(char NumberName)
{
    int Number;

    std::cout << "Enter the number '" << NumberName <<"' : ";
    std::cin >> Number;
    std::cout << "\n";

    return Number;
}

int main()
{
    int a = 0;
    int b = 0;

    a = NumberReading('a');
    b = NumberReading('b');

    std::cout << "(" << a << " + " << b << ")^2 = ";
    std::cout << squared_amount(a, b) << "\n";
}
